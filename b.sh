go mod vendor -e -o 3rdparty/go
bazelisk run //:gazelle -- update -go_prefix=portal.io -external vendored
. ./user.env
bazelisk run //3rdparty/cargo:crates_vendor -- --repin
exec bazelisk "$@"