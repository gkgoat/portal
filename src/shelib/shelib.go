package shelib

import (
	"os"

	shell "github.com/ipfs/go-ipfs-api"
)

var Sh = shell.NewShell(os.Getenv("IPFS_URL"))
