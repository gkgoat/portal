load("@rules_proto//proto:defs.bzl", "proto_library")
load("@io_bazel_rules_go//proto:def.bzl", "go_proto_library")

# Copyright 2017 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

load("//bazel:grpc_build_system.bzl", "grpc_package", "grpc_proto_library")
load("//bazel:python_rules.bzl", "py_proto_library")

licenses(["notice"])

grpc_package(
    name = "core",
    visibility = "public",
)

grpc_proto_library(
    name = "stats_proto",
    srcs = ["stats.proto"],
)

proto_library(
    name = "stats_descriptor",
    srcs = ["stats.proto"],
    visibility = ["//visibility:public"],
)

py_proto_library(
    name = "stats_py_pb2",
    deps = [":stats_descriptor"],
)

go_proto_library(
    name = "grpc_core_go_proto",
    importpath = "portal.io/3rdparty/grpc/src/proto/grpc/core",
    proto = ":stats_descriptor",
    visibility = ["//visibility:public"],
)
