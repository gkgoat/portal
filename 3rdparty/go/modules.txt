# bazil.org/fuse v0.0.0-20200524192727-fb710f7dfd05
## explicit; go 1.13
# github.com/bazelbuild/rules_go v0.34.0
## explicit
github.com/bazelbuild/rules_go/go/tools/bazel
github.com/bazelbuild/rules_go/go/tools/bazel_testing
github.com/bazelbuild/rules_go/go/tools/internal/txtar
github.com/bazelbuild/rules_go/tests/core/go_proto_library_importmap
# github.com/btcsuite/btcd v0.20.1-beta
## explicit; go 1.12
github.com/btcsuite/btcd/btcec
# github.com/crackcomm/go-gitignore v0.0.0-20170627025303-887ab5e44cc3
## explicit
github.com/crackcomm/go-gitignore
# github.com/gogo/protobuf v1.3.1
## explicit
github.com/gogo/protobuf/proto
# github.com/golang/protobuf v1.5.2
## explicit; go 1.9
github.com/golang/protobuf/jsonpb
github.com/golang/protobuf/proto
github.com/golang/protobuf/protoc-gen-go/descriptor
github.com/golang/protobuf/protoc-gen-go/plugin
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/empty
github.com/golang/protobuf/ptypes/struct
github.com/golang/protobuf/ptypes/timestamp
github.com/golang/protobuf/ptypes/wrappers
# github.com/gorilla/mux v1.8.0
## explicit; go 1.12
github.com/gorilla/mux
# github.com/gorilla/websocket v1.5.0
## explicit; go 1.12
github.com/gorilla/websocket
# github.com/hexops/vecty v0.6.0
## explicit; go 1.14
# github.com/ipfs/go-cid v0.0.7
## explicit; go 1.13
github.com/ipfs/go-cid
# github.com/ipfs/go-ipfs-api v0.3.0
## explicit; go 1.16
github.com/ipfs/go-ipfs-api
github.com/ipfs/go-ipfs-api/options
# github.com/ipfs/go-ipfs-files v0.0.9
## explicit; go 1.16
github.com/ipfs/go-ipfs-files
# github.com/klauspost/compress v1.10.3
## explicit; go 1.13
github.com/klauspost/compress/flate
# github.com/libp2p/go-buffer-pool v0.0.2
## explicit; go 1.12
github.com/libp2p/go-buffer-pool
# github.com/libp2p/go-flow-metrics v0.0.3
## explicit; go 1.12
github.com/libp2p/go-flow-metrics
# github.com/libp2p/go-libp2p-core v0.6.1
## explicit; go 1.13
github.com/libp2p/go-libp2p-core/crypto
github.com/libp2p/go-libp2p-core/crypto/pb
github.com/libp2p/go-libp2p-core/metrics
github.com/libp2p/go-libp2p-core/peer
github.com/libp2p/go-libp2p-core/peer/pb
github.com/libp2p/go-libp2p-core/protocol
github.com/libp2p/go-libp2p-core/record
github.com/libp2p/go-libp2p-core/record/pb
# github.com/libp2p/go-openssl v0.0.7
## explicit; go 1.12
github.com/libp2p/go-openssl
github.com/libp2p/go-openssl/utils
# github.com/minio/blake2b-simd v0.0.0-20160723061019-3f5f724cb5b1
## explicit
github.com/minio/blake2b-simd
# github.com/minio/sha256-simd v0.1.1
## explicit; go 1.12
github.com/minio/sha256-simd
# github.com/mitchellh/go-homedir v1.1.0
## explicit
github.com/mitchellh/go-homedir
# github.com/mr-tron/base58 v1.2.0
## explicit; go 1.12
github.com/mr-tron/base58/base58
# github.com/multiformats/go-base32 v0.0.3
## explicit
github.com/multiformats/go-base32
# github.com/multiformats/go-base36 v0.1.0
## explicit; go 1.11
github.com/multiformats/go-base36
# github.com/multiformats/go-multiaddr v0.3.0
## explicit; go 1.13
github.com/multiformats/go-multiaddr
github.com/multiformats/go-multiaddr/net
# github.com/multiformats/go-multibase v0.0.3
## explicit; go 1.11
github.com/multiformats/go-multibase
# github.com/multiformats/go-multihash v0.0.14
## explicit; go 1.13
github.com/multiformats/go-multihash
# github.com/multiformats/go-varint v0.0.6
## explicit; go 1.12
github.com/multiformats/go-varint
# github.com/spacemonkeygo/spacelog v0.0.0-20180420211403-2296661a0572
## explicit
github.com/spacemonkeygo/spacelog
# github.com/spaolacci/murmur3 v1.1.0
## explicit
github.com/spaolacci/murmur3
# github.com/whyrusleeping/tar-utils v0.0.0-20180509141711-8c6c8ba81d5c
## explicit
github.com/whyrusleeping/tar-utils
# go.opencensus.io v0.22.4
## explicit; go 1.13
go.opencensus.io/internal/tagencoding
go.opencensus.io/metric/metricdata
go.opencensus.io/metric/metricproducer
go.opencensus.io/resource
go.opencensus.io/stats
go.opencensus.io/stats/internal
go.opencensus.io/stats/view
go.opencensus.io/tag
# golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
## explicit; go 1.11
golang.org/x/crypto/blake2b
golang.org/x/crypto/blake2s
golang.org/x/crypto/curve25519
golang.org/x/crypto/internal/subtle
golang.org/x/crypto/nacl/box
golang.org/x/crypto/nacl/secretbox
golang.org/x/crypto/poly1305
golang.org/x/crypto/salsa20/salsa
golang.org/x/crypto/sha3
# golang.org/x/net v0.0.0-20220809184613-07c6da5e1ced
## explicit; go 1.17
golang.org/x/net/context
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/timeseries
golang.org/x/net/trace
# golang.org/x/sys v0.0.0-20220808155132-1c4a2a72c664
## explicit; go 1.17
golang.org/x/sys/cpu
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.7
## explicit; go 1.17
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# google.golang.org/genproto v0.0.0-20220810155839-1856144b1d9c
## explicit; go 1.19
google.golang.org/genproto/googleapis/api/annotations
google.golang.org/genproto/googleapis/bytestream
google.golang.org/genproto/googleapis/rpc/code
google.golang.org/genproto/googleapis/rpc/status
google.golang.org/genproto/googleapis/type/color
google.golang.org/genproto/protobuf/field_mask
google.golang.org/genproto/protobuf/ptype
google.golang.org/genproto/protobuf/source_context
# google.golang.org/grpc v1.48.0
## explicit; go 1.14
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/grpclb/state
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/channelz
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/credentials/insecure
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancer/gracefulswitch
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/credentials
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpclog
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/grpcutil
google.golang.org/grpc/internal/metadata
google.golang.org/grpc/internal/pretty
google.golang.org/grpc/internal/resolver
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/resolver/unix
google.golang.org/grpc/internal/serviceconfig
google.golang.org/grpc/internal/status
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/internal/transport/networktype
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/protobuf v1.28.1
## explicit; go 1.11
google.golang.org/protobuf/encoding/protojson
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/json
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/emptypb
google.golang.org/protobuf/types/known/fieldmaskpb
google.golang.org/protobuf/types/known/sourcecontextpb
google.golang.org/protobuf/types/known/structpb
google.golang.org/protobuf/types/known/timestamppb
google.golang.org/protobuf/types/known/typepb
google.golang.org/protobuf/types/known/wrapperspb
google.golang.org/protobuf/types/pluginpb
# nhooyr.io/websocket v1.8.7
## explicit; go 1.13
nhooyr.io/websocket
nhooyr.io/websocket/internal/bpool
nhooyr.io/websocket/internal/errd
nhooyr.io/websocket/internal/wsjs
nhooyr.io/websocket/internal/xsync
