package mesh

import (
	"context"
	"fmt"
)

type MeshServer struct {
	UnimplementedRouterServer
	LocalExec func(string) (string, error)
	peers     []RouterClient
}

var Self string

func (s *MeshServer) Route(ctx context.Context, in RoutedMessage) (*RoutedReply, error) {
	if in.id == Self {
		t, err := s.LocalExec(in.content)
		return &RoutedReply{content: t}, err
	}
	for _, p := range s.peers {
		x, err := p.Route(ctx, in)
		if err == nil {
			return x, nil
		}
	}
	return nil, fmt.Errorf("No peers found")
}
