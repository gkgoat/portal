#include <cstdint>
#include <memory>
#include <map>
#include <unicorn/unicorn.h>
#include <linux/limits.h>
struct lbe_sysdata {
    std::map<int, int> fmap;
};
template <typename R>
inline void mmap_mem(R r, uc_engine *uc, void *&addr, size_t length, int prot,
                     int flags, int fd, off_t offset) {
  uc_mem_map(uc, addr, length, mmap(0, length, prot, flags, r(fd), offset),
             prot);
}
namespace {
void hook_syscall(uc_engine *uc, void *dat) {
  std::shared_ptr<lbe_sysdata> h((lbe_sysdata *)dat);
  uint64_t sysn;
  uc_reg_read(uc, UC_X86_REG_RAX, &sysn);
  if(sysn == SYS_OPENAT){
    int gdfd;
    uint64_t pbg;
    char path[PATH_MAX];
    int flags;
    mode_t mod;
    uc_reg_read(uc, UC_X86_REG_RBX, &gdfd);
    uc_reg_read(uc, UC_X86_REG_RCX, &pbg);
    uc_reg_read(uc, UC_X86_REG_RDX, &flags);
    uc_reg_read(uc, UC_X86_REG_R8, &mod);
    uc_mem_read(uc, pbg, &path, PATH_MAX);
    int r = openat(h->fmap[gdfd], append("/scp/sandbox/rw",abspath(path)), flags, mode);
    h->fmap[r] = r;
    uc_reg_write(uc, UC_X86_REG_RAX, r);
  }
  if(sysn == SYS_MMAP){
    void *ta;
    size_t l;
    int p,f,d;
    off_t s;
    uc_reg_read(uc, UC_X86_REG_RBX, &ta);
    uc_reg_read(uc, UC_X86_REG_RCX, &l);
    uc_reg_read(uc, UC_X86_REG_RDX, &p);
    uc_reg_read(uc, UC_X86_REG_R8, &f);
    uc_reg_read(uc, UC_X86_REG_R9, &d);
    uc_reg_read(uc, UC_X86_REG_R10, &s);
    mmap_mem([&](int a){return h->fmap[a];}, uc, ta, l, p, f, d, s);
    uc_reg_write(uc, UC_X86_REG_RAX, ta);
  }
}
} // namespace
inline std::shared_ptr<uc_engine> init() {
  auto uc = std::make_shared<uc_engine>();
  uc_open(UC_ARCH_X86, UC_MODE_64, &uc);
  uc_hook_add(uc, &trace1, UC_HOOK_INSN, hook_syscall,
              std::make_shared<lbe_sysdata>(), 1, 0, UC_X86_INS_SYSCALL);
  return uc;
}