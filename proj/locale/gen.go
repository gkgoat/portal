package main

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/bazelbuild/rules_go/go/tools/bazel"
)

//go:embed all
var all string

func main() {
	locales := map[string]map[string]string{}
	s := strings.Split(all, "\n")
	for _, l := range s {
		r, _ := bazel.Runfile(fmt.Sprintf("lang/%s.json", l))
		f, _ := os.Open(r)
		defer f.Close()
		var n map[string]string
		json.NewDecoder(f).Decode(&n)
		for k, v := range n {
			lk, ok := locales[k]
			if !ok {
				locales[k] = map[string]string{}
				lk = locales[k]
				lk[l] = v
			}
		}
	}
	for l, m := range locales {
		fmt.Printf("static char* $loc$%s(char *$){", l)
		for n, v := range m {
			fmt.Printf("if(!strcmp($,%s))return %s;", strconv.Quote(n), strconv.Quote(v))
		}
		fmt.Println("}")
	}
}
