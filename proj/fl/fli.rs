
use rand::{seq::SliceRandom, RngCore, SeedableRng, Rng}; // 0.6.5
use rand_chacha::ChaChaRng;
use unicode_segmentation::UnicodeSegmentation;
use std::{collections::{HashSet, HashMap}, ops::BitXor}; // 0.1.1
#[macro_use]
extern crate lazy_static;
pub fn dshuffle(s: &mut (impl SliceRandom + ?Sized), d: u8) {
    let seed = [d; 32];
    let mut rng = ChaChaRng::from_seed(seed);
    s.shuffle(&mut rng);
}
pub fn split_vowels(s: &str) -> (String, String) {
    (
        s.chars()
            .filter(|c| !['a', 'e', 'i', 'o', 'u'].contains(&c.to_lowercase().nth(0).unwrap()))
            .collect(),
        s.chars()
            .filter(|c| ['a', 'e', 'i', 'o', 'u'].contains(&c.to_lowercase().nth(0).unwrap()))
            .collect(),
    )
}
pub fn split_puncts(s: &str) -> (String, String) {
    (
        s.chars()
            .filter(|c| !['.', ',', '!', '?'].contains(c))
            .collect(),
        s.chars()
            .filter(|c| ['.', ',', '!', '?'].contains(c))
            .collect(),
    )
}
lazy_static::lazy_static! {
    pub static ref iach: String = {
        let mut n: u8 = 0;
        let mut s = ('a'..='z').collect::<Vec<char>>();
        for cc in "AEIOU".chars(){
            s.push(cc);
        };
        {
            let ss: &mut [char] = &mut s;
            dshuffle(ss,n);
            n += 1;
        }
        while !split_vowels( &s.iter().collect::<String>()).1.starts_with("aeiou"){
        let ss: &mut [char] = &mut s;
        dshuffle(ss,n);
        n = n.wrapping_add(1);
        }
        let s: String = s.into_iter().collect();
        let (c,v) = split_vowels(&s);
        return format!("{}{}",v,c)
    };
    pub static ref rach: String = ({
        let mut n: u8 = 0;
        let mut s = ('𖩀'..='𖩞').collect::<Vec<char>>();
        while rmp("av", &s.iter().collect::<String>(), &iach) != "𖩅𖩍"{
        let ss: &mut [char] = &mut s;
        dshuffle(ss,n);
        n = n.wrapping_add(1);
        }
        s.into_iter().collect()
    });
    pub static ref iallsyls: Vec<String> = {
        let (c,v) = split_vowels(&iach);
        c.chars().flat_map(|x|v.chars().map(|y|vec![x,y].iter().collect()).collect::<Vec<String>>()).collect::<Vec<String>>()
    };
    pub static ref rallsyls: Vec<String> = iallsyls.iter().map(|x|rans(&*x,false)).collect();
}
pub fn rmp(sa: &str, ky: &str, kx: &str) -> String {
    return UnicodeSegmentation::graphemes(sa, true)
        .map(|x: &str| -> String {
            return (match UnicodeSegmentation::graphemes(kx, true).position(|y|y == x) {
                None => x,
                Some(i) => match UnicodeSegmentation::graphemes(ky, true).nth(i) {
                    None => x,
                    Some(y) => y,
                },
            }).to_owned();
        })
        .fold("".to_owned(), |a,b|format!("{}{}",a,b))
}
pub fn rans(x: &str, toLatin: bool) -> String {
    return rmp(
        x,
        if !toLatin { &*rach } else { &*iach },
        if !toLatin { &*iach } else { &*rach },
    );
}