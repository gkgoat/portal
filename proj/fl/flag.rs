
use rand::{seq::SliceRandom, RngCore, SeedableRng, Rng}; // 0.6.5
use rand_chacha::ChaChaRng;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::iter::Extend;
use std::collections::HashMap;
use rand::distributions::uniform::SampleRange;
use unicode_segmentation::UnicodeSegmentation;

#[macro_use]
extern crate lazy_static;
lazy_static!{
    pub static ref baseShuffle: Vec<String> = {
        let mut r = fli_base::iallsyls.clone();
        let mut n: u8 = 0;
        while r[0] != "bE"{
            let ss: &mut [String] = &mut r;
            fli_base::dshuffle(ss,n);
            n = n.wrapping_add(1);
        };
        {
            let ss: &mut [String] = &mut r;
            fli_base::dshuffle(ss,0);
        };
        r
    };
    pub static ref special: Vec<String> = {
        let mut s = baseShuffle.clone();
        {
            let ss: &mut [String] = &mut s;
            fli_base::dshuffle(ss, 0);
        }
        s.into_iter().take(46).collect()
    };
    pub static ref bases: Vec<String> = {
        baseShuffle.clone().into_iter().filter(|y|!special.contains(y)).collect()
    };
    pub static ref fused: Vec<String> = fuse(bases.clone(),0,700);
    pub static ref wbase: Vec<String> = {
        fused.clone().into_iter().flat_map(|x|if calculate_hash(&x) % 2 == 0{vec![x]}else{special[0..7].iter().map(|y|format!("{}{}",x,y)).collect()}).flat_map(|x|std::iter::repeat(x.clone()).take((calculate_hash(&(x.clone() + "_")) as usize) % (11 - x.len()) as usize)).collect()
    };
    pub static ref wfmark: Vec<String> = {
        let mut r = wbase.clone();
        let seed = [51; 32];
        let mut rng = ChaChaRng::from_seed(seed);
        let mut n: u8 = rng.gen();
        // let mut m = HashMap::new();
        // for _ in 0..32{
        //     for w in wbase.clone(){
        //         let mx = m.entry(w.clone()).or_insert(0usize);
        //         if rng.gen_range(0..(18 - log_2(*mx + 1))) == 0{
        //             r.push(w);
        //             *mx += 1;
        //         }
        //         *mx += 2;
        //     }
        // }
        while !vet1(&r){
            for _ in 0..8{
                let ss: &mut [String] = &mut r;
                fli_base::dshuffle(ss,n);
                n = n.wrapping_add(1);
            }
            r.push(r[rng.gen_range(0..r.len())].clone());
        };
        r
    };
}
const fn num_bits<T>() -> usize {
    std::mem::size_of::<T>() * 8
}
pub fn log_2(x: usize) -> usize {
    num_bits::<usize>() as usize - (x.leading_zeros() as usize) - 1
}
fn prepend<T>(v: Vec<T>, s: &[T]) -> Vec<T>
where
    T: Clone,
{
    let mut tmp: Vec<_> = s.to_owned();
    tmp.extend(v);
    tmp
}
fn fuse(mut x: Vec<String>,offset: usize,num: usize) -> Vec<String>{
    let mut res = vec![];
    let seed = [51; 32];
    let mut rng = ChaChaRng::from_seed(seed);
    let mut n: u8 = rng.gen();
    while res.len() < num{
        {
            let ss: &mut [String] = &mut x;
            fli_base::dshuffle(ss,n);
            n = n.wrapping_add(rng.gen_range(1..4));
        }
        let mut w = "".to_owned();
        let r = rng.gen_range(4..6) - log_2(rng.gen_range(2..16)) + offset;
        for _ in 0..r{
            if let Some(y) = x.pop(){
                w = format!("{}{}",w,y);
                if y == y.to_lowercase(){
                    x = prepend(x, &[y.clone(),y.clone()]);
                }
                x = prepend(x, &[y.clone(),y.clone(),y]);
            }
        }
        res.push(w);
    }
    return res;
}
fn freqB(a: &[String], b: &str) -> usize{
    let mut r = 0;
    for c in a{
        let mut b: Vec<&str> = b.graphemes(true).collect();
        b.pop();
        b.pop();
        let mut c: Vec<&str> = c.graphemes(true).collect();
        c.pop();
        c.pop();
        if c == b{
            r += 1;
        }
    }
    return r
}
fn vet1(x: &[String]) -> bool{
    let mut f: usize = 0;
    for (i,v) in x.iter().map(|x|x.clone()).enumerate(){
        let ni = (i + 1) % x.len();
        let n = x[ni].clone();
        let pi = (i + x.len() - 1) % x.len();
        let p = x[pi].clone();
        let l: isize = (v.len() as isize) - (n.len() as isize);
        let m = l.abs() as usize;
        let o: isize = (v.len() as isize) - (p.len() as isize);
        let pm = o.abs() as usize;
        let t: isize = (p.len() as isize) - (n.len() as isize);
        let tm = t.abs() as usize;
        if m == 0 && pm == 0{
            f += 1;
        }
        if tm > 6{
            f += 2;
        }
    };
    // print!("v");
    return f < (x.len() * 2 / (20 - log_2(x.len())));
}
fn monoidal<A: Clone,B: Clone>(a: Vec<A>, b: Vec<B>) -> Vec<(A,B)>{
    return a.into_iter().flat_map(|va|b.iter().map(|vb|(va.clone(),vb.clone())).collect::<Vec<_>>()).collect()
}
fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}
fn hard_range(x: u64, r: impl SampleRange<usize>) -> usize{
    let seed: [u8; 32] = unsafe{
        std::mem::transmute::<[u64; 4], _>([x; 4])
    };
    let mut rng = ChaChaRng::from_seed(seed);
    return rng.gen_range(r)
}
fn main(){
    print!("//");
    for s in baseShuffle.clone(){
        print!("{} ",s);
    };
    println!("");
    print!("//{} ",bases.len());
    for s in bases.clone(){
        print!("{} ",s);
    };
    println!("");
    print!("//{} ",fused.len());
    for s in fused.clone(){
        print!("{} ",s);
    };
    println!("");
    print!("//{} ",wbase.len());
    for s in wbase.clone(){
        print!("{} ",s);
    };
    println!("");
    print!("//{} ",wfmark.len());
    for s in wfmark.clone(){
        print!("{} ",s);
    };
    println!("");
    println!("lazy_static!{{");

    println!("}}");
}