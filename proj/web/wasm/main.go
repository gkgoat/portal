package main

import (
	"context"
	"net"
	"net/http"
	"syscall/js"

	shell "github.com/ipfs/go-ipfs-api"
	"nhooyr.io/websocket"
)

func tcp(n, x string) (net.Conn, error) {
	j := js.Global
	s := j.Get("tcp_endpoint").String()
	c, _, err := websocket.Dial(context.Background(), s+"?tgt="+s, nil)
	if err != nil {
		return nil, err
	}
	return websocket.NetConn(context.Background(), c, websocket.MessageBinary), nil
}

var Htr = &http.Transport{Dial: tcp}
var Sh = shell.NewShellWithClient(js.Global.Get("ipfs_url").String(), http.Client{Transport: Htr})

func main() {
}
