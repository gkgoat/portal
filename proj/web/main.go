package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"portal.io/src/shelib"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/$", func(w http.ResponseWriter, r *http.Request) {
		for _, x := range r.URL.Query()["cid"] {
			ip, err := shelib.Sh.Cat(x)
			if err == nil {
				io.Copy(w, ip)
			}
		}
	})
	r.HandleFunc("/tcp", func(w http.ResponseWriter, r *http.Request) {
		t, err := net.Dial("tcp", r.URL.Query()["tgt"][0])
		if err != nil {
			return
		}
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			return
		}
		for {
			messageType, r, err := conn.NextReader()
			if err != nil {
				return
			}
			w, err := conn.NextWriter(messageType)
			if err != nil {
				return
			}
			if _, err := io.Copy(t, r); err != nil {
				return
			}
			if _, err := io.Copy(w, t); err != nil {
				return
			}
			if err := w.Close(); err != nil {
				return
			}
		}
	})
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), nil))
}
