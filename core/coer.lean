universe u
def fnAt[BEq a](x: a)(fn: b -> b) (inp: a -> b) (v: a) := if x == v then fn (inp v) else inp v
def Vec (n : Nat) (α : Type _) : Type _
  := Fin n → α
namespace vec
def hd {n: Nat} (v: Vec (n + 1) a): (a) := v $ Fin.ofNat 0
def tl {n: Nat} (v: Vec (n + 1) a) (val: Fin n): (a) := v $ Fin.succ val
def toList {n: Nat} (v: Vec n a): List a := match n with
  | 0 => []
  | n + 1 => hd v :: toList (tl v)
#check @hd
def concat (x: Vec n String): String := List.foldl (λ x y => x ++ y) "" $ toList x
def map {n: Nat} (fn: a -> b) (v: Vec n a): Vec n b := λy => fn (v y)
def fromList (l: List a): Vec (List.length l) a := λx => List.get l x
instance : Functor (Vec n) where
  map := map
end vec
def Slice (a: Type _): Type _ := (n: Nat) × Vec n a
namespace slice
def slength (x: Slice a): Nat := x.fst
def toVec(x: Slice a): Vec (slength x) a := x.snd
def fromVec(x: Vec n a): Slice a := Sigma.mk n x
def toList(x: Slice a): List a := vec.toList (toVec x)
def fromList(x: List a): Slice a := fromVec (vec.fromList x)
def map(fn: a -> b) (a: Slice a): Slice b := fromVec (vec.map fn (toVec a))
instance : Functor Slice where
  map := map
instance : Monad Slice where
  pure {a: Type _} (x: a) := fromList [x]
  bind {a b: Type _} (x: Slice a) (fn: a -> Slice b) := fromList (List.foldl (List.append) [] (toList (map (λy => toList (fn y)) x)))

end slice
def But (b: Type _) (a: b) : Type _ := (v: b) × (PLift (Not (v = a)))
namespace but
def un(x: But b x): b := x.fst
def mk{x: b} (v: b) {prf: Not (v = x)}: But b x := Sigma.mk v (PLift.up prf)

end but
def Only {b: Type _} (a: b) : Type _ := (v: b) × (PLift (v = a))
namespace only
def it(x: b): Only x := Sigma.mk x (PLift.up (Eq.refl x))
end only
class NatTr (a: Type _ -> Type _) (b: Type _ -> Type _) where
  tr : a c -> b c

instance : NatTr a a where
  tr := id

instance : NatTr (Vec n) Slice where
  tr := slice.fromVec

instance : NatTr Slice List where
  tr := slice.toList
inductive Either (a: Type _) (b: Type _) where
| left (h: a): Either a b
| right (h: b): Either a b
def Either.elim (y: a -> c) (z: b -> c) (x: Either a b): c := match x with
| Either.left a => y a
| Either.right b => z b
class CoApplicative(f: Type u -> Type u) extends Functor f where
  choose : f (Either a b) -> Either (f a) (f b)
  extract : f a -> a
class CoMonad (w: Type u -> Type u) extends CoApplicative w where
  dup : w a -> w (w a)
  choose x := match extract x with
   | Either.left _ => Either.left $ map (Either.elim id sorry) x
   | Either.right _ => Either.right $ map (Either.elim sorry id) x
def extend [CoMonad w] (f: w a -> b): w a -> w b := Function.comp (Functor.map f) (CoMonad.dup)
class Inject (w: Type u -> Type u) extends CoMonad w where
  inject : w a -> a -> w a
instance {a: Type u} : CoMonad.{u} (Prod a) where
  extract := Prod.snd
  dup x := Prod.mk (Prod.fst x) x
  map fn x := Prod.mk (Prod.fst x) (fn (Prod.snd x))
instance {a: Type u} : Inject.{u} (Prod a) where
  inject a x := (a.fst, x)
def PStream(a: Type _) : Type _ := Nat -> a
namespace pstreams
def hed(x: PStream a): a := x 0
def tel(x: PStream a) (b: Nat) := x (b + 1)
def cons(x: Unit -> PStream a) (y: Unit -> a) (b: Nat) := if b == 0 then y () else x () (b - 1)
instance : Functor PStream where
  map f x v := f (x v)
-- def psdup (x: PStream a): PStream (PStream a) := cons (λ _ => psdup (tel x)) (λ _ => x)
-- termination_by psdup x => x
-- decreasing_by sorry -- SAFETY: function is lazy and will never loop and thus return false
instance : CoMonad PStream where
 extract := hed
 dup x y z := x (y + z)
instance : Inject PStream where
 inject a x v := if v == 0 then x else a v
def slice(x: PStream a)( b: Fin n): a := x b
def srepeat(x: Vec (n + 1) a) (b: Nat): a := x (Fin.ofNat b)
def injectAt (b: PStream a) (n: Nat) (x: a) (y: Nat) := if y == n then x else b y
end pstreams
def PTape(x: Type _): Type _ := Int -> x
namespace ptapes
instance : Functor PTape where
  map f x v := f (x v)
instance : CoMonad PTape where
  extract x := x 0
  dup x y z := x (y + z)
instance : Inject PTape where
 inject a x v := if v == 0 then x else a v
instance : NatTr PTape PStream where
  tr x y := x y
instance : NatTr PStream PTape where
  tr x y := x (Int.natAbs y)
def ptsdupl (x: PTape v) (a: Int) (b: Nat) := x (a + b)
def ptsdupr (x: PTape v) (a: Nat) (b: Int) := x (a + b)
def tel(x: PTape a): PTape a := ptsdupr x 1
def cotel(x: PTape a) := CoMonad.dup x (-1)
def injectAt(n: Int)(x: PTape a) (y: a): PTape a := CoMonad.dup (Inject.inject (CoMonad.dup x n) y) (0 - n)
end ptapes
def Store(x: Type _) (a: Type _) := Prod (x -> a) x
instance : Functor (Store s) where
 map fn x := (Function.comp fn x.fst,x.snd)
instance {s: Type u} : CoMonad.{u} (Store s) where
 extract x := x.fst x.snd
 dup x := (Prod.mk x.fst,x.snd)
instance {s: Type u} [BEq s] : Inject.{u} (Store s) where
 inject a x := (λ y => if y == a.snd then x else a.fst y, a.snd)

class Contravariant (f: Type _ -> Type _) where
 contramap : (a -> b) -> f b -> f a

class Invariant (f: Type _ -> Type _) where
 invmap : Prod (a -> b) (b -> a) -> Prod (f a -> f b) (f b -> f a)

instance [Functor f] : Invariant f where
 invmap f := (Functor.map f.fst,Functor.map f.snd)

instance [Contravariant f] : Invariant f where
 invmap f := (Contravariant.contramap f.snd,Contravariant.contramap f.fst)

def Set (α : Type u) := α → Prop

instance : Contravariant Set where
  contramap fn x := Function.comp x fn

namespace Set

def mem (x : α) (a : Set α) := a x

infix:50 (priority := high) "∈" => mem

theorem setext {a b : Set α} (h : ∀ x, x ∈ a ↔ x ∈ b) : a = b :=
  funext (fun x => propext (h x))

def power (x: Set a) (v: Set a): Prop := ∀(y: a), v y -> x y

def just (x: a) (y: a): Prop := x = y

def of (x: b -> a) (y: a): Prop := ∃(c: b), x c = y

def ofList (l: List.{u} a) := of (slice.fromList.{u} l).snd

end Set

def empty : Set α := fun x => False

notation (priority := high) "∅" => empty

def inter (a b : Set α) : Set α :=
  fun x => x ∈ a ∧ x ∈ b

infix:70 " ∩ " => inter

theorem inter_self (a : Set α) : a ∩ a = a :=
  Set.setext fun x => Iff.intro
    (fun ⟨h, _⟩ => h)
    (fun h => ⟨h, h⟩)

theorem inter_empty (a : Set α) : a ∩ ∅ = ∅ :=
  Set.setext fun x => Iff.intro
    (fun ⟨_, h⟩ => h)
    (fun h => False.elim h)

theorem empty_inter (a : Set α) : ∅ ∩ a = ∅ :=
  Set.setext fun x => Iff.intro
    (fun ⟨h, _⟩ => h)
    (fun h => False.elim h)

theorem inter.comm (a b : Set α) : a ∩ b = b ∩ a :=
  Set.setext fun x => Iff.intro
    (fun ⟨h₁, h₂⟩ => ⟨h₂, h₁⟩)
    (fun ⟨h₁, h₂⟩ => ⟨h₂, h₁⟩)

