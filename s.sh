p=add
while getopts "p" o; do
    case "${o}" in
        p)
            p=pull
            ;;
    esac
done
shift $((OPTIND-1))
git subtree $p -P "$@"
echo "git subtree pull -P $@" >> pull.sh