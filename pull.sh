
git subtree pull -P 3rdparty/rules_go https://github.com/bazelbuild/rules_go.git master
cd tools; wget https://github.com/dhappy/git-remote-ipfs/raw/lang/javascript/git-remote-ipfs;wget https://github.com/dhappy/git-remote-ipfs/raw/lang/javascript/index.js; cd ..;
git subtree pull -P 3rdparty/rules_nodejs https://github.com/bazelbuild/rules_nodejs.git stable
git subtree pull -P 3rdparty/rules_appimage https://github.com/lalten/rules_appimage.git main
git subtree pull -P 3rdparty/bazel_deps https://github.com/mjbots/bazel_deps.git master
git subtree pull -P 3rdparty/libfuse https://android.googlesource.com/platform/external/libfuse master
git subtree pull -P 3rdparty/ipfs-http-cc https://github.com/vasild/cpp-ipfs-http-client.git master
cd 3rdparty/json; wget https://github.com/nlohmann/json/raw/develop/single_include/nlohmann/json.hpp; cd ../..;
git subtree pull -P 3rdparty/grpc https://github.com/grpc/grpc.git master
git subtree pull -P 3rdparty/gitfs https://github.com/presslabs/gitfs.git master
git subtree pull -P 3rdparty/gitfs https://github.com/presslabs/gitfs.git master
git subtree pull -P 3rdparty/gitfs https://github.com/taylorjacklespriggs/gitfs.git pygit2-1.6.1
